/*
 * Video
 */

(function($) {
    $(document).ready(function() {
    /* IF YOU WANT TO APPLY SOME BASIC JQUERY TO REMOVE THE VIDEO BACKGROUND ON A SPECIFIC VIEWPORT MANUALLY
     var is_mobile = false;
    if( $('.player').css('display')=='none') {
        is_mobile = true;
    }
    if (is_mobile == true) {
        //Conditional script here
        $('.big-background, .small-background-section').addClass('big-background-default-image');
    }else{
        $(".player").mb_YTPlayer();
    }
    });
*/
    /*  IF YOU WANT TO USE DEVICE.JS TO DETECT THE VIEWPORT AND MANIPULATE THE OUTPUT  */

        //Device.js will check if it is Tablet or Mobile - http://matthewhudson.me/projects/device.js/
        if (!device.tablet() && !device.mobile()) {
            $(".player").mb_YTPlayer();
        } else {
            //jQuery will add the default background to the preferred class
            $('.video-background').addClass(
                'video-background-default-image');
        }

        // $('.btn-send').click(function()
        // {
        //     $('#thanks').arcticmodal();
        // });
        
        var body = $('body');
        
        var submitQuestionnaireForm = function (rewrite) {
            var drink = $('#drink');
            var visit = $('#visit');
            
            var data = {
                visit: $(visit).val(),
                drink: $(drink).val(),
                rewrite: rewrite
            };
            
            console.log(data);

            $.ajaxSetup({
                headers: { "X-CSRFToken":  $.cookie('csrftoken')}
            });

            $.post('/feedback/', data
            )
            .success(function(data){
                console.log("Success!");
                $.arcticmodal('close');
                $('#thanks').arcticmodal();
                $('select#visit option:selected').removeAttr('selected');
                $('select#drink option:selected').removeAttr('selected');
            })
            .error(function(data){
                data = JSON.parse(data['responseText']);
                if (data['error_code'] == 400001) {
                    $('#already-filled').arcticmodal();
                }
            })
        };

        $(body).on('click', '#btn-send', function () {
            var drink = $('#drink');
            var visit = $('#visit');
            var error = false;
            if ($(drink).val() == -1) {
                $(drink).addClass('has-error');
                error = true;
            } else {
                $(drink).removeClass('has-error');
            }

            if ($(visit).val() == -1) {
                $(visit).addClass('has-error');
                error = true;
            } else {
                $(visit).removeClass('has-error');
            }
            if (error == true) return;

            $.get('/feedback/check/'
            )
            .success(function(data){
                submitQuestionnaireForm(false);
            })
            .error(function(data){
                data = JSON.parse(data['responseText']);
                if (data['error_code'] == 400001) {
                    $('#already-filled').arcticmodal();
                }
            })

        });
        
        $(body).on('click', '#rewrite-submit', function () {
            submitQuestionnaireForm(true);
        });

        $(body).on('click', '#rewrite-cancel', function () {
            $('#already-filled').arcticmodal('close');
            $('select#visit option:selected').removeAttr('selected');
            $('select#drink option:selected').removeAttr('selected');
        });

    });
})(jQuery);


/*
 * Countdown
 */

$('#clock').countdown('2016/06/18').on('update.countdown', function(event) {
       var $this = $(this).html(event.strftime(''
         + '<div><span>%-w</span>нед</div>'
         + '<div><span>%-d</span>дн</div>'
         + '<div><span>%H</span>ч</div>'
         + '<div><span>%M</span>мин</div>'
         + '<div><span>%S</span>сек</div>'));
     });