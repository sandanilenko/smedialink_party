"""
Development settings
"""
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CORS_ORIGIN_ALLOW_ALL = True

DEBUG = True

TEMPLATE_DEBUG = True

THUMBNAIL_DEBUG = False

ALLOWED_HOSTS = ('http://smedialinkparty.sandan.me',)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, '../../db.sqlite3'),
    }
}


EMAIL_HOST = ''
EMAIL_PORT = 587
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = ''



ACCESS_KEY_ID = ''
SECRET_ACCESS_KEY = ''
BUCKET=''
AWS_S3_FORCE_HTTP_URL = True

BROKER_URL = 'amqp://ansbook:qwerty@localhost:5672/localhost'

LOG_LEVEL = 'DEBUG'

ANSBOOK_CLIENT_ID = u''
ANSBOOK_CLIENT_SECRET = u''
