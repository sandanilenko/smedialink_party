"""
Development settings
"""

CORS_ORIGIN_ALLOW_ALL = True

DEBUG = True

TEMPLATE_DEBUG = True

THUMBNAIL_DEBUG = False

ALLOWED_HOSTS = ('http://localhost:8000',)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'ansbook',                    # Or path to database file if using sqlite3.
        'USER': 'ansbook',                      # Not used with sqlite3.
        'PASSWORD': 'qwerty',                  # Not used with sqlite3.
        'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '5432',                      # Set to empty string for default. Not used with sqlite3.
    }
}


EMAIL_HOST = ''
EMAIL_PORT = 587
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = ''



ACCESS_KEY_ID = ''
SECRET_ACCESS_KEY = ''
BUCKET=''
AWS_S3_FORCE_HTTP_URL = True

BROKER_URL = 'amqp://ansbook:qwerty@localhost:5672/localhost'

LOG_LEVEL = 'DEBUG'

ANSBOOK_CLIENT_ID = u''
ANSBOOK_CLIENT_SECRET = u''
