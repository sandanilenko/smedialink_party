__author__ = 'slyfest'

import requests
from exceptions import ApiStandartException

def decode(point_str):
    coord_chunks = [[]]
    for char in point_str:
        value = ord(char) - 63
        split_after = not (value & 0x20)
        value &= 0x1F
        coord_chunks[-1].append(value)

        if split_after:
            coord_chunks.append([])

    del coord_chunks[-1]
    coords = []

    for coord_chunk in coord_chunks:
        coord = 0
        for i, chunk in enumerate(coord_chunk):
            coord |= chunk << (i * 5)
        if coord & 0x1:
            coord = ~coord
        coord >>= 1
        coord /= 100000.0
        coords.append(coord)

    points = []
    prev_x = 0
    prev_y = 0

    for i in xrange(0, len(coords) - 1, 2):
        if coords[i] == 0 and coords[i + 1] == 0:
            continue
        prev_x += coords[i + 1]
        prev_y += coords[i]
        points.append((round(prev_x / 10, 6), round(prev_y / 10, 6)))

    return points


def encodeValue(coordinate, factor):
    coordinate = int(round(coordinate * factor));
    coordinate <<= 1
    if (coordinate < 0):
        coordinate = ~coordinate;
    output = ''
    while (coordinate >= 0x20):
        output += unichr((0x20 | (coordinate & 0x1f)) + 63);
        coordinate >>= 5
    output += unichr(coordinate + 63)
    return output


def encode_z(coords):
    if len(coords) == 0:
        return ''
    precision = 6
    factor = pow(10, precision | 6)
    output = encodeValue(coords[0][0], factor) + encodeValue(coords[0][1], factor);

    for i in range(1, len(coords)):
        a = coords[i]
        b = coords[i - 1]
        output += encodeValue(a[0] - b[0], factor)
        output += encodeValue(a[1] - b[1], factor)
    return output


def get_route(start_lat, start_lng, destination_lat, destination_lng):
    ACCESS_TOKEN = 'pk.eyJ1Ijoid2VpYmFvIiwiYSI6Iks5MGx6dXMifQ.Tt79_pcPtPWo1reeMZ6l-A'
    BASE_URL = 'http://api.tiles.mapbox.com/v4/directions/mapbox.driving/'
    data = '{0},{1};{2},{3}.json'.format(start_lng, start_lat, destination_lng, destination_lat)

    params = {'access_token': ACCESS_TOKEN, 'geometry': 'polyline'}
    r = requests.get(BASE_URL + data, params)
    r.raise_for_status()
    json_data = r.json()

    if json_data['routes']:
        distance = json_data['routes'][0]['distance']
        coords = json_data['routes'][0]['geometry']

        result = {'distance': distance, 'route': coords}
        return result
    raise ApiStandartException(dict(error_code=400067))


def get_route_g(start_lat, start_lng, destination_lat, destination_lng):
    API_KEY = 'AIzaSyCTNEIw2q7CeW7cra9tVhfjslUpk9sb7xY'
    URL = 'https://maps.googleapis.com/maps/api/directions/json'

    params = {'key': API_KEY, 'origin': str(start_lat) + ',' + str(start_lng),
              'destination': str(destination_lat) + ',' + str(destination_lng),
              'alternatives': 'true'}

    r = requests.get(URL, params)
    r.raise_for_status()
    json_data = r.json()

    if json_data['routes']:
        polyline = json_data['routes'][0]['overview_polyline']['points']
        distance = json_data['routes'][0]['legs'][0]['distance']['value']
        result = {'distance': distance, 'route': polyline}
    else:
        result = {}
    return result


def get_route_osrm(start_lat, start_lng, destination_lat, destination_lng):
    URL = 'http://router.project-osrm.org/viaroute'
    data = '?loc={0},{1}&loc={2},{3}'.format(start_lat, start_lng, destination_lat, destination_lng)
    r = requests.get(URL + data)
    r.raise_for_status()
    json_data = r.json()

    return dict(
        distance=json_data['route_summary']['total_distance'],
        route=json_data['route_geometry']
    )

