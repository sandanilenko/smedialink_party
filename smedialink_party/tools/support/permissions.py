from rest_framework import permissions
from rest_framework.permissions import BasePermission
from support.exceptions import ApiStandartException

__author__ = 'sandan'


class CanAddOrder(BasePermission):
    """
    Allows access for add order only to customers.
    """

    def has_permission(self, request, view):
        if request.method not in permissions.SAFE_METHODS:
            has_perm = request.user.has_perms(['orders.add_order'])
            if not has_perm:
                raise ApiStandartException(dict(error_code=400007))
        return True



