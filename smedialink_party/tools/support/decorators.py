from django.conf.urls import include

__author__ = 'sandan'


def decorated_include(urls, decorator):
    """Used to decorate all urls in a 3rd party app with a specific decorator"""
    urls_to_decorate = include(urls)
    for url_pattern in urls_to_decorate[0].urlpatterns:
        url_pattern._callback = decorator(url_pattern._callback)
    return urls_to_decorate