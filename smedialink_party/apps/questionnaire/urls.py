from django.conf.urls import url
from .views import IndexView
from .api import FeedbackView, CheckSingleFeedback

urlpatterns = [
    url(r'^$', IndexView.as_view()),
    url(r'feedback/check/$', CheckSingleFeedback.as_view()),
    url(r'feedback/$', FeedbackView.as_view()),
]
