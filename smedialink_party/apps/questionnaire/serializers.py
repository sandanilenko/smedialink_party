from support.exceptions import ApiStandartException
from support.serializers import CustomModelSerializer
from rest_framework.serializers import BooleanField
from .models import Feedback


class FeedbackSerializer(CustomModelSerializer):
    rewrite = BooleanField(required=False)

    class Meta:
        model = Feedback
        fields = ('visit', 'drink', 'rewrite')

    def create(self, user, **validated_data):
        is_rewrite = False
        if 'rewrite' in validated_data:
            is_rewrite = validated_data.pop('rewrite')
        try:
            feedback = self.Meta.model.objects.get(user=user)
            if is_rewrite:
                feedback.delete()
            else:
                raise ApiStandartException(dict(error_code=400001))
        except self.Meta.model.DoesNotExist:
            pass

        validated_data['user'] = user
        self.Meta.model.objects.create(**validated_data)