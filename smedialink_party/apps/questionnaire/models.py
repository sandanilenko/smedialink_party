from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db.models import Model, CharField, TextField, EmailField, DateTimeField, BooleanField, PositiveIntegerField, \
    ManyToManyField, ForeignKey

from custom_auth.models import CustomUser


class Feedback(Model):
    VISIT_ITEMS = (
        (1, 'да'),
        (2, 'нет')
    )

    DRINK_ITEMS = (
        (1, 'вода'),
        (2, 'лимонад'),
        (3, 'вино'),
        (4, 'энергетики'),
        (5, 'газировка'),
        (6, 'глинтвейн'),
        (7, 'сок'),
    )

    visit = PositiveIntegerField(choices=VISIT_ITEMS, verbose_name=_('visit'))
    drink = PositiveIntegerField(choices=DRINK_ITEMS, verbose_name=_('drink'))
    user = ForeignKey(CustomUser, blank=True, null=True, verbose_name=_('user'))

    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'feedback')
        verbose_name_plural = _(u'feedback')

    def __unicode__(self):
        return u'name - {0}, email - {1}'.format(self.name, self.email)