from rest_framework.response import Response
from rest_framework.views import APIView
from support.exceptions import ApiStandartException

from .serializers import FeedbackSerializer, Feedback


class FeedbackView(APIView):
    def post(self, request):
        serializer = FeedbackSerializer(data=request.data)
        serializer.is_valid()
        serializer.create(request.user, **serializer.validated_data)
        return Response()


class CheckSingleFeedback(APIView):
    def get(self, request):
        try:
            Feedback.objects.get(user=request.user)
            raise ApiStandartException(dict(error_code=400001))
        except Feedback.DoesNotExist:
            return Response()