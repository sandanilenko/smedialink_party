from django.contrib.admin import ModelAdmin, register

from .models import Feedback


@register(Feedback)
class FeedbackAdmin(ModelAdmin):
    list_display = ('user', 'drink', 'visit', 'created_at')