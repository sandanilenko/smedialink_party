# coding=utf-8
from django.forms import ImageField
from pytz import timezone
from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.auth.hashers import make_password
# from strgen import StringGenerator as SG
from django.db.models import CharField, BooleanField, DateTimeField, Model, PositiveSmallIntegerField, OneToOneField, \
    DecimalField, ForeignKey, DateField, URLField
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
# from oauth2_provider.models import AccessToken
from django.contrib.contenttypes.models import ContentType

# from queue.send_email import SendEmail
from support.exceptions import ApiStandartException

__author__ = 'sandan'


class CustomUserManager(BaseUserManager):
    def create_user(self, social_id=None, password=None):
        """
        Creates and saves a User with the given password.
        """
        user = self.model(
            is_staff=False,
            is_active=True,
            is_superuser=False,
            social_id=social_id,
            date_joined=datetime.utcnow().replace(tzinfo=timezone('UTC'))
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, social_id, password):
        """
        Creates and saves a superuser with the given password.
        """
        user = self.model(
            social_id=social_id,
            password=make_password(password),
            is_staff=True,
            is_active=True,
            is_superuser=True,
            date_joined=datetime.utcnow().replace(tzinfo=timezone('UTC'))
        )
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    social_id = CharField(max_length=128, unique=True, db_index=True, primary_key=True, verbose_name=_('social id'))
    first_name = CharField(max_length=255, blank=True, null=True, verbose_name=_('first name'))
    last_name = CharField(max_length=255, blank=True, null=True, verbose_name=_('last_name'))
    avatar = URLField(max_length=2048, blank=True, null=True, verbose_name=_('avatar'))

    is_staff = BooleanField(default=False, verbose_name=_('admin'))
    date_joined = DateTimeField(default=datetime.utcnow, verbose_name=_('created at'))

    is_active = BooleanField(default=True, verbose_name=_('is active'))
    is_confirmed = BooleanField(default=False, verbose_name=_('is confirmed'))

    USERNAME_FIELD = 'social_id'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __unicode__(self):
        return self.get_username()

    def get_username(self):
        return getattr(self, self.USERNAME_FIELD)

    def get_full_name(self):
        return "%s" % self.social_id

    def get_short_name(self):
        return "%s" % self.social_id

    def get_avatar(self):
        return self.avatar or settings.DEFAULT_USER_AVATAR

    # def send_email-_for_registration(self):
    #     SendEmail.send.delay(
    #         dict(
    #             data=dict(
    #                 subject_template=settings.CONFIRM_REGISTRATION_LETTER_SUBJECT,
    #                 template_text=settings.CONFIRM_REGISTRATION_LETTER_TXT,
    #                 template_html=settings.CONFIRM_REGISTRATION_LETTER_HTML,
    #                 from_email=settings.DEFAULT_FROM_EMAIL,
    #                 emails_to=[self.email],
    #                 params=dict(
    #                     activation_url=settings.FRONT_CONFIRM_REGISTRATION_URL.format(
    #                         self.user_confirmation.filter(is_activated=False).first().key
    #                     ),
    #                     file_server_url=settings.FILE_SERVER_URL,
    #                     user=self
    #                 )
    #             )
    #         ))
