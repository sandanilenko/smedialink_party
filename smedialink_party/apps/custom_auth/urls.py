from django.conf.urls import url
from .views import AuthView, VKAuthView

urlpatterns = [
    url(r'^auth/$', AuthView.as_view()),
    url(r'^auth-vk/$', VKAuthView.as_view())
]