# -*- coding: utf-8 -*-
from urllib import request
from urllib.parse import urlencode
from django import http
from django.contrib.auth import login
from django.http import Http404
from django.views.generic import TemplateView, RedirectView
from django.conf import settings
import json

from .models import CustomUser


class AuthView(TemplateView):
    template_name = 'authentication.html'

    def get_context_data(self, **kwargs):
        context = super(AuthView, self).get_context_data(**kwargs)
        context['vk_auth_link'] = settings.VK_AUTH.format(
            client_id=settings.VK_APP_ID,
            redirect_url=settings.VK_LOGIN_REDIRECT_URL
        )
        return context


def send_request_get_method(url, content):
    data = urlencode(content)
    data = data.encode('ascii')
    with request.urlopen(url, data) as f:
        r_data = f.read().decode('utf-8')
        return r_data


class VKAuthView(RedirectView):
    url = '/'

    def get(self, request, *args, **kwargs):
        if 'code' in request.GET:
            code = request.GET['code']
            content = dict(
                code=code,
                client_id=settings.VK_APP_ID,
                client_secret=settings.VK_APP_SECRET,
                redirect_uri=settings.VK_LOGIN_REDIRECT_URL
            )
            try:
                data = json.loads(send_request_get_method(settings.URL_VK_GET_ACCESS_TOKEN, content))
            except:
                raise Http404
            content = dict(
                user_ids=data['user_id'],
                fields=u'photo_400_orig,first_name,last_name',
                name_case=u'Nom',
                version=5.28,
                access_token=data['access_token']
            )
            user_data = json.loads(send_request_get_method(settings.VK_GET_USERS_URL, content), encoding='utf-8')
            if user_data['response'][0]['photo_400_orig']:
                user_avatar_url = user_data['response'][0]['photo_400_orig']
            else:
                user_avatar_url = settings.DEFAULT_USER_AVATAR

            user, created = CustomUser.objects.get_or_create(
                social_id=data['user_id'],
                defaults=dict(
                    first_name=user_data['response'][0]['first_name'],
                    last_name=user_data['response'][0]['last_name'],
                    avatar=user_avatar_url
                )
            )

            if created:
                user.first_name=user_data['response'][0]['first_name'],
                user.ast_name = user_data['response'][0]['last_name'],
                user.avatar = user_data['response'][0]['photo_400_orig']
                user.save()
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)

            url = self.get_redirect_url(*args, **kwargs)
            return http.HttpResponseRedirect(url)



