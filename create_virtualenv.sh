#!/bin/bash
#
# Creates virtualenv
#

PROJECT_NAME="smedialink_party"

virtualenv --prompt="(${PROJECT_NAME}) " -p python3 .virtualenv
source .virtualenv/bin/activate && pip install -r requirements.txt
