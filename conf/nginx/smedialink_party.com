 server {
        server_name smedialinkparty.sandan.me;

        listen 80;

        client_max_body_size 4G;

        access_log /var/log/smedialink_party/access.log;
        error_log /var/log/smedialink_party/error.log debug;

        location /static/ {
            alias /var/www/smedialink_party/smedialink_party/static/;
        }

        location /media/ {
            alias /var/www/smedialink_party/smedialink_party/media/;
        }

        location / {
                proxy_set_header Host $http_host;
                proxy_set_header X-Forwarded-Host $server_name;
                proxy_pass http://127.0.0.1:8012;
                proxy_redirect off;
                proxy_set_header X-Real-IP $remote_addr;
                add_header P3P 'CP="ALL DSP COR PSAa PSDa OUR NOR ONL UNI COM NAV"';
        }
 }